import { Outlet, Link } from 'react-router-dom';
import { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Header, Sidebar, UserCard, Footer } from '@components';
import { getUser } from '@redux/reducers/auth';
import { Row, Col, Button } from 'antd';

const LayoutSale = () => {
  const dispatch = useDispatch();
  const [title, setTitle] = useState('Daftar Jual');
  const { data: user, loading: userLoading } = useSelector((state) => state.auth.user);
  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  return (
    <Box className="layout-general">
      <Helmet onChangeClientState={(newState) => setTitle(newState.title)} />
      <Header isSale />
      <Box className="layout-content">
        <Box className="container py-3 md:py-10 max-w-[968px]">
          <h1 className="text-xl font-bold mb-6 hidden md:block">{title}</h1>
          <UserCard
            user={user}
            loading={userLoading}
            button={
              <Link to="/user/account/edit">
                <Button ghost type="primary" className="rounded-lg">
                  Edit
                </Button>
              </Link>
            }
          />
          <Row className="pt-2 md:pt-3" gutter={24}>
            <Col span={24} md={8}>
              <Sidebar loading={userLoading} />
            </Col>
            <Col span={24} md={16}>
              <Outlet />
            </Col>
          </Row>
        </Box>
      </Box>
      <Footer></Footer>
    </Box>
  );
};

export default LayoutSale;
