const offerStatus = {
  0: {
    name: 'Ditolak',
    status: 'error',
    color: '#ff4d4f'
  },
  1: {
    name: 'Pending',
    status: 'default',
    color: '#faad14'
  },
  2: {
    name: 'Proses',
    status: 'processing',
    color: '#1890ff'
  },
  3: {
    name: 'Sukses',
    status: 'success',
    color: '#52c41a'
  }
};

export const offerStatusParser = (status) => {
  return offerStatus[status];
};
