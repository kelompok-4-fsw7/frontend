import { Outlet } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { ReactComponent as Logo } from '@assets/svg/logo-auth.svg';
import { BackNavigation, Box } from '@components';

const LayoutAuth = () => {
  return (
    <Box className="layout-auth">
      <Box className="layout-auth-navigation">
        <BackNavigation />
      </Box>
      <Box className="layout-auth-wrapper">
        <Box className="layout-auth-right">
          <Link to="/">
            <Logo className="layout-auth-logo" />
          </Link>
        </Box>
        <Box className="layout-auth-left">
          <Box className="layout-auth-form">
            <Outlet />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default LayoutAuth;
