import { Helmet } from 'react-helmet';
import { useEffect } from 'react';
import { Box, ProductCard, ProductAdd } from '@components';
import { useDispatch, useSelector } from 'react-redux';
import { getProductsByOwner } from '@redux/reducers/product';
import Skeleton from 'antd/lib/skeleton';

function SaleList() {
  const dispatch = useDispatch();
  const { data: products, loading: productsLoading } = useSelector(
    (state) => state.product.productsByOwner
  );

  useEffect(() => {
    dispatch(getProductsByOwner());
  }, [dispatch]);

  return (
    <>
      <Helmet>
        <title>Daftar Jual</title>
      </Helmet>

      <Box className="product-grid-sale">
        {!productsLoading && (
          <Box>
            <ProductAdd />
          </Box>
        )}

        {productsLoading && (
          <>
            {Array(3)
              .fill(1)
              .map((i) => (
                <Skeleton key={i} className="skeleton-product" active />
              ))}
          </>
        )}
        {!productsLoading &&
          products.map((product, i) => <ProductCard product={product} key={i}></ProductCard>)}
      </Box>
    </>
  );
}

export default SaleList;
