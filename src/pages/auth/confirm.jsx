import { Helmet } from 'react-helmet';
import { useEffect } from 'react';
import { Typography } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { FiLoader } from 'react-icons/fi';
import { Box } from '@components';
import { confirm } from '@redux/reducers/auth';
import useQuery from '@libs/query';

const { Paragraph, Title } = Typography;

function Confirm() {
  const { success, loading } = useSelector((state) => state.auth.confirm);
  const { token } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  let query = useQuery();
  const email = query.get('email');

  useEffect(() => {
    if (success === true) {
      navigate('/login', { replace: true });
    }
  }, [success]); // eslint-disable-line

  useEffect(() => {
    dispatch(
      confirm({
        token,
        email
      })
    );
  }, [dispatch]); // eslint-disable-line

  return (
    <>
      <Box className="layout-auth-form-wrapper">
        <Title className="layout-auth-title" level={3}>
          Konfirmasi Akun
        </Title>
        <Paragraph>Mohon bersabar, ini ujian.</Paragraph>
        {loading && <FiLoader size={22} className="animate-spin" />}
      </Box>

      <Helmet>
        <title>Konfirmasi Akun</title>
      </Helmet>
    </>
  );
}

export default Confirm;
