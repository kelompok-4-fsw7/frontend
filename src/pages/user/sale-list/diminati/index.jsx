import { Helmet } from 'react-helmet';
import { useEffect } from 'react';
import { Empty, Box, ProductCard } from '@components';
import { useDispatch, useSelector } from 'react-redux';
import { getProductLiked } from '@redux/reducers/product_like';
import { Skeleton } from 'antd';

function Diminati() {
  const dispatch = useDispatch();
  const { data: products, loading: productsLoading } = useSelector(
    (state) => state.product_like.liked
  );

  useEffect(() => {
    dispatch(getProductLiked());
  }, [dispatch]);

  return (
    <>
      <Helmet>
        <title>Daftar Diminati</title>
      </Helmet>

      {!productsLoading && products?.length < 1 && (
        <Box>
          <Empty
            className="mx-auto"
            description="Belum ada produk yang kamu sukai. Kalo kamu suka, belum tentu dia juga suka"
          />
        </Box>
      )}

      <Box className="product-grid-sale">
        {productsLoading && (
          <>
            <Skeleton className="w-full h-48 skeleton-product" active />
            <Skeleton className="w-full h-48 skeleton-product" active />
            <Skeleton className="w-full h-48 skeleton-product" active />
          </>
        )}
        {!productsLoading &&
          products.map((product, i) => (
            <ProductCard product={product.product} key={i}></ProductCard>
          ))}
      </Box>
    </>
  );
}

export default Diminati;
