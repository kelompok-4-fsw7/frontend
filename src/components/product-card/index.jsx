import { Card } from 'antd';
import { Link } from 'react-router-dom';
import { Box } from '@components';
import { formatRupiah } from '@libs/number';

const { Meta } = Card;

const ProductCard = ({ product }) => {
  return (
    <Box>
      <Link to={'/' + product?.slug}>
        <Card
          className="product-card"
          hoverable
          cover={
            <img alt="example" src={product?.images[0]?.url || 'https://picsum.photos/200/300'} />
          }
        >
          <Meta
            title={product?.name}
            description={
              <Box>
                <Box className="ant-card-meta-category">{product?.category?.name}</Box>
                <Box className="ant-card-meta-price">{formatRupiah(product?.price)}</Box>
              </Box>
            }
          />
        </Card>
      </Link>
    </Box>
  );
};

export default ProductCard;
