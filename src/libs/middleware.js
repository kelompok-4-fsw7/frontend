import { Navigate, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { getToken, getUser } from '@redux/reducers/auth';

export function Authenticated({ children }) {
  const location = useLocation();

  if (getToken() === null) {
    return <Navigate to="/login" state={{ from: location }} />;
  }

  return children;
}

export function Guest({ children }) {
  const location = useLocation();

  if (getToken() !== null) {
    return <Navigate to="/" state={{ from: location }} />;
  }

  return children;
}

export function UserValid({ children }) {
  const { data: user, loading: userLoading } = useSelector((state) => state.auth.user);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]); //eslint-disable-line

  if (!userLoading && user?.status < 2) {
    return <Navigate to="/user/account/edit" />;
  }
  return children;
}
