import { MemoryRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';
import Box from '../index';

it('renders box correctly', () => {
  const tree = renderer
    .create(
      <MemoryRouter>
        <Box>Test Box</Box>
      </MemoryRouter>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
