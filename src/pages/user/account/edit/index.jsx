import { Helmet } from 'react-helmet';
import { Button, Form, Input, Select } from 'antd';
import { FiCamera, FiLoader } from 'react-icons/fi';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, BackNavigation } from '@components';
import { getUser, updateUser } from '@redux/reducers/auth';
import { getCities } from '@redux/reducers/city';
import { useUpload } from '@libs/upload';

const { TextArea } = Input;
const { Option } = Select;

const Profile = () => {
  const [form] = Form.useForm();
  const [image, setImage] = useState(null);
  const [imageLoading, setImageLoading] = useState(false);
  const { handleUpload } = useUpload();
  const dispatch = useDispatch();
  const { data: user } = useSelector((state) => state.auth.user);
  const { data: cities } = useSelector((state) => state.city.cities);
  const { loading: updateUserLoading } = useSelector((state) => state.auth.updateUser);
  useEffect(() => {
    dispatch(getUser());
    dispatch(getCities());
  }, [dispatch]);

  const onFinish = async (values) => {
    dispatch(
      updateUser({
        ...values,
        status: 2,
        image
      })
    );
  };

  useEffect(() => {
    setImage(user?.image);
  }, [user]);

  const formInitialValues = {
    name: user?.name,
    city_id: user?.city_id,
    phone: user?.phone,
    address: user?.address
  };

  const handleUploadAvatar = async (e) => {
    e.persist();
    setImage(null);
    setImageLoading(true);
    const { data: result } = await handleUpload(e);
    setImageLoading(false);
    setImage(result?.file);
  };

  return (
    <>
      <Box className="container profile">
        <BackNavigation className="profile-back" />
        <Box className="profile-img cursor-pointer">
          <label htmlFor="avatar-uploader">
            {image ? (
              <img className="profile-img-avatar cursor-pointer" src={image} alt="profile" />
            ) : (
              <Box className="profile-img-icon cursor-pointer">
                {imageLoading ? (
                  <FiLoader size={22} className="animate-spin" />
                ) : (
                  <FiCamera size={22} />
                )}
              </Box>
            )}

            <input
              id="avatar-uploader"
              style={{
                visibility: 'hidden',
                width: 0,
                height: 0
              }}
              type="file"
              onChange={handleUploadAvatar}
            />
          </label>
        </Box>
        {formInitialValues?.name ? (
          <Form
            form={form}
            layout="vertical"
            autoComplete="off"
            initialValues={formInitialValues}
            onFinish={onFinish}
          >
            <Form.Item
              name="name"
              label="Nama"
              rules={[{ required: true, message: 'Nama harus diisi' }]}
            >
              <Input placeholder="Nama" />
            </Form.Item>
            <Form.Item
              name="city_id"
              label="Kota"
              rules={[{ required: true, message: 'Kota harus diisi' }]}
            >
              <Select>
                {cities?.map((city) => (
                  <Option key={city?.id} value={city?.id}>
                    {city?.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="address"
              label="Alamat"
              rules={[{ required: true, message: 'Alamat harus diisi' }]}
            >
              <TextArea rows={2} placeholder="Contoh: Jalan Ikan Hiu 33" />
            </Form.Item>
            <Form.Item
              name="phone"
              label="No Handphone"
              rules={[{ required: true, message: 'No Telepon harus diisi' }]}
            >
              <Input placeholder="contoh: +628123456789" />
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                block
                size="large"
                disable={updateUserLoading}
                loading={updateUserLoading}
                htmlType="submit"
              >
                Simpan
              </Button>
            </Form.Item>
          </Form>
        ) : null}
      </Box>
      <Helmet>
        <title>Lengkapi Info Akun</title>
      </Helmet>
    </>
  );
};

export default Profile;
