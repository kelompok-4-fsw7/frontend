import { Helmet } from 'react-helmet';
import { Button, Form, Input, Select, Row, Col } from 'antd';
import { FiPlus, FiLoader, FiTrash } from 'react-icons/fi';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { Box, BackNavigation } from '@components';

import { getCategories } from '@redux/reducers/category';
import { createProduct, reset } from '@redux/reducers/product';
import { useUpload } from '@libs/upload';
const { TextArea } = Input;
const { Option } = Select;

const CreateProduct = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { handleUpload } = useUpload();
  const [images, setImages] = useState([]);
  const [status, setStatus] = useState(0);
  const [imageLoading, setImageLoading] = useState(false);
  const [form] = Form.useForm();
  const { data: categories } = useSelector((state) => state.category.category);
  const {
    data: product,
    loading: createProductLoading,
    success: successCreateProduct
  } = useSelector((state) => state.product.createProduct);
  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);

  useEffect(() => {
    if (successCreateProduct) {
      dispatch(reset());
      navigate(`/${product.slug}`);
    }
  }, [successCreateProduct]); //eslint-disable-line

  const onFinish = async (values) => {
    setTimeout(() => {
      dispatch(
        createProduct({
          ...values,
          status,
          images
        })
      );
    }, 500);
  };

  const handleUploadImage = async (e) => {
    e.persist();
    setImageLoading(true);
    const { data: result } = await handleUpload(e, 'products');
    let img = images.concat(result.file);
    setImageLoading(false);
    setImages(img);
  };

  const handleDeleteImage = (url) => {
    let img = images.filter((x) => x !== url);
    setImages(img);
  };

  return (
    <>
      <Box className="product container">
        <BackNavigation className="profile-back" />
        <Form form={form} layout="vertical" autoComplete="off" onFinish={onFinish}>
          <Form.Item
            name="name"
            label="Nama Produk"
            rules={[
              {
                required: true,
                message: 'Nama produk harus diisi'
              }
            ]}
          >
            <Input placeholder="Nama Produk" />
          </Form.Item>
          <Form.Item
            name="price"
            label="Harga Produk"
            rules={[
              {
                required: true,
                message: 'Harga produk harus diisi'
              }
            ]}
          >
            <Input type="number" placeholder="Rp 0,00" />
          </Form.Item>
          <Form.Item
            name="category_id"
            label="Kategori"
            rules={[
              {
                required: true,
                message: 'Kategori produk harus diisi'
              }
            ]}
          >
            <Select placeholder="Pilih Kategori">
              {categories?.map((category) => (
                <Option value={category?.id}>{category?.name}</Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name="description"
            label="Deskripsi"
            rules={[
              {
                required: true,
                message: 'Deskripsi produk harus diisi'
              }
            ]}
          >
            <TextArea rows={2} placeholder="Contoh: Jalan Ikan Hiu 33" />
          </Form.Item>

          <Form.Item label="Foto Produk">
            <Box className="flex">
              {images?.length < 4 && (
                <label htmlFor="uploader">
                  <Box to="#" className="add-image">
                    <Box className="add-image-icon">
                      {imageLoading ? (
                        <FiLoader size={22} className="animate-spin" />
                      ) : (
                        <FiPlus size={22} />
                      )}
                    </Box>
                  </Box>
                  <input
                    id="uploader"
                    style={{
                      visibility: 'hidden',
                      width: 0,
                      height: 0
                    }}
                    type="file"
                    onChange={handleUploadImage}
                  />
                </label>
              )}

              {images?.map((image) => (
                <Box className="relative">
                  <img src={image} className="add-image-result" alt="" />
                  <FiTrash
                    onClick={() => {
                      handleDeleteImage(image);
                    }}
                    className="absolute right-2 top-2 text-red-800 cursor-pointer"
                  />
                </Box>
              ))}
            </Box>
          </Form.Item>

          <Form.Item>
            <Row gutter={16}>
              <Col span={12}>
                <Button
                  type="secondary"
                  disabled={createProductLoading}
                  loading={createProductLoading}
                  htmlType="submit"
                  block
                  size="large"
                  onClick={() => setStatus(0)}
                >
                  Preview
                </Button>
              </Col>
              <Col span={12}>
                <Button
                  type="primary"
                  disabled={createProductLoading}
                  loading={createProductLoading}
                  htmlType="submit"
                  block
                  size="large"
                  onClick={() => setStatus(1)}
                >
                  Terbitkan
                </Button>
              </Col>
            </Row>
          </Form.Item>
        </Form>
      </Box>
      <Helmet>
        <title>Tambah Produk</title>
      </Helmet>
    </>
  );
};

export default CreateProduct;
