import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';

import { Empty, Box, ProductOfferCard } from '@components';
import { getOffersByUser, getOffersBySeller } from '@redux/reducers/offer';
import { Tabs, Skeleton } from 'antd';
const { TabPane } = Tabs;

const Penawaran = () => {
  const dispatch = useDispatch();
  const { data: offersMade, loading: offersMadeLoading } = useSelector(
    (state) => state.offer.offersMade
  );

  const { data: offersReceived, loading: offersReceivedLoading } = useSelector(
    (state) => state.offer.offersReceived
  );

  useEffect(() => {
    dispatch(getOffersByUser());
    dispatch(getOffersBySeller());
  }, [dispatch]);

  return (
    <>
      <Helmet>
        <title>Daftar Penawaran</title>
      </Helmet>
      <Tabs defaultActiveKey="1">
        <TabPane tab="Penawaran Diterima" key="1">
          {offersReceivedLoading && (
            <>
              <Skeleton className="w-full h-48" active />
            </>
          )}
          {!offersReceivedLoading && offersReceived?.length < 1 && (
            <Box>
              <Empty className="my-4 mx-auto" description="Belum ada tawaran yang kamu terima" />
            </Box>
          )}
          {!offersReceivedLoading &&
            offersReceived.map((offer, i) => <ProductOfferCard offer={offer} />)}
        </TabPane>
        <TabPane tab="Penawaran Dibuat" key="2">
          {offersMadeLoading && (
            <>
              <Skeleton className="w-full h-48" active />
            </>
          )}
          {!offersMadeLoading && offersMade?.length < 1 && (
            <Box>
              <Empty className="my-4 mx-auto" description="Belum ada tawaran yang kamu buat" />
            </Box>
          )}
          {!offersMadeLoading && offersMade.map((offer, i) => <ProductOfferCard offer={offer} />)}
        </TabPane>
      </Tabs>
    </>
  );
};

export default Penawaran;
