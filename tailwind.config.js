/** @type {import('tailwindcss').Config} */
module.exports = {
  layers: ['components'],
  content: ['./src/**/*.{html,js,jsx,css}'],
  theme: {
    extend: {
      boxShadow: {
        default: '0px 0px 10px rgba(0, 0, 0, 0.15)',
        mini: '0px 0px 4px rgba(0, 0, 0, 0.15)'
      },
      container: {
        center: true
      },
      maxWidth: {
        xl: '568px'
      },
      colors: {
        primary: '#7126B5',
        gray: {
          200: '#EEEEEE',
          350: '#8A8A8A',
          850: '#151515'
        },
        red: {
          600: '#FA2C5A'
        },
        purple: {
          200: '#E2D4F0',
          800: '#7126B5'
        }
      }
    }
  },
  plugins: []
};
