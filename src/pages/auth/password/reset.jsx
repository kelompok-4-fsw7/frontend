import { Helmet } from 'react-helmet';
import { Typography } from 'antd';
import { Link } from 'react-router-dom';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Form, Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@components';
import { resetPassword, reset } from '@redux/reducers/password';
import useQuery from '@libs/query';

const { Paragraph, Title } = Typography;

function ResetPassword() {
  let query = useQuery();
  const email = query.get('email');
  const token = query.get('token');

  const { success, loading } = useSelector((state) => state.password.reset);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onFinish = async (values) => {
    dispatch(
      resetPassword({
        email,
        token,
        ...values
      })
    );
  };

  const [form] = Form.useForm();

  useEffect(() => {
    if (success === true) {
      form.resetFields();
      dispatch(reset());
      navigate('/login');
    }
  }, [success]); // eslint-disable-line

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
      <Title className="layout-auth-title" level={3}>
        Reset Password
      </Title>
      <Form
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          name="newPassword"
          label="Password baru"
          rules={[
            { required: true, message: 'Password baru harus diisi' },
            { min: 8, message: 'Password minimal 8 karakter' }
          ]}
        >
          <Input.Password placeholder="Password baru" />
        </Form.Item>
        <Form.Item
          name="confirmNewPassword"
          label="Konfirmasi password baru"
          rules={[
            { required: true, message: 'Konfirmasi password baru harus diisi' },
            { min: 8, message: 'Password minimal 8 karakter' },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('newPassword') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('Konfirmasi password baru tidak sama'));
              }
            })
          ]}
        >
          <Input.Password placeholder="Konfirmasi password baru" />
        </Form.Item>
        <Form.Item>
          <Button
            loading={loading}
            disabled={loading}
            htmlType="submit"
            type="primary"
            block
            size="large"
          >
            Reset Password
          </Button>
        </Form.Item>
      </Form>

      <Box className="layout-auth-info">
        <Paragraph>
          Kembali ke halaman{' '}
          <Link to="/">
            <strong>utama</strong>
          </Link>
        </Paragraph>
      </Box>

      <Helmet>
        <title>Reset Password</title>
      </Helmet>
    </>
  );
}

export default ResetPassword;
