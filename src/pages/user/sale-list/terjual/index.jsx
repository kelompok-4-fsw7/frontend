import { Helmet } from 'react-helmet';
import { useEffect } from 'react';
import { Box, ProductCard, Empty } from '@components';
import { useDispatch, useSelector } from 'react-redux';
import { getProductsByOwnerSold } from '@redux/reducers/product';
import Skeleton from 'antd/lib/skeleton';

function Terjual() {
  const dispatch = useDispatch();
  const { data: products, loading: productsLoading } = useSelector(
    (state) => state.product.productsByOwnerSold
  );

  useEffect(() => {
    dispatch(getProductsByOwnerSold());
  }, [dispatch]);
  return (
    <>
      <Helmet>
        <title>Daftar Terjual</title>
      </Helmet>
      {!productsLoading && products?.length < 1 && (
        <Box>
          <Empty
            className="mx-auto"
            description="Belum ada produkmu yang terjual nih, sabar ya rejeki nggak kemana kok"
          />
        </Box>
      )}

      <Box className="product-grid-sale">
        {productsLoading && (
          <>
            <Skeleton className="w-full h-48 skeleton-product" active />
            <Skeleton className="w-full h-48 skeleton-product" active />
            <Skeleton className="w-full h-48 skeleton-product" active />
          </>
        )}
        {!productsLoading &&
          products.map((product, i) => <ProductCard product={product} key={i}></ProductCard>)}
      </Box>
    </>
  );
}

export default Terjual;
