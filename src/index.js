import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { store } from '@redux/store';
import App from './App';
import reportWebVitals from './reportWebVitals';

import 'react-toastify/dist/ReactToastify.css';
import 'antd/dist/antd.min.css';
import '@assets/css/tailwind.css';
import '@assets/css/antd.less';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <>
    <Provider store={store}>
      <App />
    </Provider>
    <ToastContainer closeOnClick autoClose={5000} />
  </>
);

reportWebVitals();
