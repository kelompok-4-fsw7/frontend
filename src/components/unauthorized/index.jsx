import { Box } from '@components';
import { ReactComponent as UnauthorizedImg } from '@assets/svg/unauthorized.svg';

const Unauthorized = ({ description }) => {
  return (
    <Box className="unauthorized  ">
      <UnauthorizedImg className="unauthorized-image" />

      <Box className="unauthorized-text">{description}</Box>
    </Box>
  );
};

export default Unauthorized;
