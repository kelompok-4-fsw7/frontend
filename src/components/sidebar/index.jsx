import { FiBox, FiChevronRight, FiHeart, FiDollarSign, FiShuffle, FiList } from 'react-icons/fi';
import { Skeleton } from 'antd';
import { Box } from '@components';

import { NavLink } from 'react-router-dom';

const Sidebar = ({ loading = false }) => {
  return loading ? (
    <Box className="sidebar-wrapper-desktop">
      <Skeleton.Button className="skeleton-sidebar" active />
    </Box>
  ) : (
    <>
      <Box className="sidebar-wrapper sidebar-wrapper-desktop">
        <h1 className="sidebar-title">Kategori</h1>
        <ul className="sidebar-list-wrapper">
          <li>
            <NavLink to="/user/daftar-jual" activeClassName="active" className="link">
              <span className="sidebar-option">
                <span className="list-text">
                  <FiBox size={24} />
                  <span className="sidebar-option-text">Semua Produk</span>
                </span>
                <FiChevronRight size={24} />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/user/diminati" activeClassName="active" className="link">
              <span className="sidebar-option">
                <span className="list-text">
                  <FiHeart size={24} />
                  <span className="sidebar-option-text">Diminati</span>
                </span>
                <FiChevronRight size={24} />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/user/terjual" activeClassName="active" className="link">
              <span className="sidebar-option">
                <span className="list-text">
                  <FiDollarSign size={24} />
                  <span className="sidebar-option-text">Terjual</span>
                </span>
                <FiChevronRight size={24} />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/user/penawaran" activeClassName="active" className="link">
              <span className="sidebar-option">
                <span className="list-text">
                  <FiList size={24} />
                  <span className="sidebar-option-text">Penawaran</span>
                </span>
                <FiChevronRight size={24} />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/user/transaksi" activeClassName="active" className="link">
              <span className="sidebar-option">
                <span className="list-text">
                  <FiShuffle size={24} />
                  <span className="sidebar-option-text">Transaksi</span>
                </span>
                <FiChevronRight size={24} />
              </span>
            </NavLink>
          </li>
        </ul>
      </Box>
      <Box className="sidebar-mobile">
        <NavLink to="/user/daftar-jual" className="category" activeClassName="active">
          <Box className="category-icon">
            <FiBox size={24} />{' '}
          </Box>
          <Box>Produk</Box>
        </NavLink>
        <NavLink to="/user/diminati" className="category" activeClassName="active">
          <Box className="category-icon">
            <FiHeart size={24} />{' '}
          </Box>
          <Box>Diminati</Box>
        </NavLink>
        <NavLink to="/user/terjual" className="category" activeClassName="active">
          <Box className="category-icon">
            <FiDollarSign size={24} />{' '}
          </Box>
          <Box>Terjual</Box>
        </NavLink>
        <NavLink to="/user/penawaran" className="category" activeClassName="active">
          <Box className="category-icon">
            <FiList size={24} />{' '}
          </Box>
          <Box>Penawaran</Box>
        </NavLink>
        <NavLink to="/user/transaksi" className="category" activeClassName="active">
          <Box className="category-icon">
            <FiShuffle size={24} />{' '}
          </Box>
          <Box>Transaksi</Box>
        </NavLink>
      </Box>
    </>
  );
};

export default Sidebar;
