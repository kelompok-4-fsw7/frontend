import { Outlet } from 'react-router-dom';
import { Box, Footer, Header } from '@components';

const LayoutGeneral = () => {
  return (
    <Box className="layout-general">
      <Header isHome />
      <Box className="layout-content">
        <Outlet />
      </Box>
      <Footer />
    </Box>
  );
};

export default LayoutGeneral;
