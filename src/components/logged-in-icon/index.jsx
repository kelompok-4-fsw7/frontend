import { FiList, FiBell, FiUser } from 'react-icons/fi';
import { Link, NavLink } from 'react-router-dom';
import { Dropdown, Skeleton } from 'antd';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getNotifications } from '@redux/reducers/notification';
import { Box, NotificationsItem } from '@components';

const Notification = ({ list, loading }) => {
  return (
    <Box className="notification p-6 bg-white rounded-2xl w-[376px] mt-4 shadow-default">
      {!loading && list?.length < 1 && (
        <Box className="text-center text-base">Belum ada notifikasi</Box>
      )}
      {!loading &&
        list?.length > 0 &&
        list?.map((item, i) => <NotificationsItem key={i} notification={item} />)}

      {!loading && list?.length > 0 && (
        <Link to="/user/notifications" className="block text-center pt-5">
          Lihat semua notifikasi
        </Link>
      )}
      {loading &&
        Array(3)
          .fill(1)
          .map((i) => <Skeleton.Button key={i} className="skeleton-notification" active />)}
    </Box>
  );
};

const LoggedInIcon = () => {
  const dispatch = useDispatch();
  const { data: notifications, loading: notificationsLoading } = useSelector(
    (state) => state.notification.notifications
  );
  useEffect(() => {
    dispatch(getNotifications({ limit: 5 }));
  }, [dispatch]); // eslint-disable-line

  return (
    <Box className="logged-icon-wrapper">
      <NavLink
        to={'/user/daftar-jual'}
        className="logged-icon-link"
        active="logged-icon-link-active"
      >
        <FiList size={24} />
        <span className="logged-icon-link-text">Daftar Jual</span>
      </NavLink>
      <NavLink
        to={'/user/notifications'}
        className="logged-icon-link  block md:hidden"
        active="logged-icon-link-active"
      >
        <span className="logged-icon-link-text">Notifikasi</span>
      </NavLink>
      <Box className="logged-icon-link cursor-pointer md:block hidden">
        <Dropdown
          overlay={<Notification list={notifications} loading={notificationsLoading} />}
          placement="bottomRight"
        >
          <FiBell size={24} />
        </Dropdown>
        <span className="logged-icon-link-text">Notifikasi</span>
      </Box>
      <NavLink to={'/user/account'} className="logged-icon-link" active="logged-icon-link-active">
        <FiUser size={24} />
        <span className="logged-icon-link-text">Akun Saya</span>
      </NavLink>
    </Box>
  );
};

export default LoggedInIcon;
