import { Typography, Avatar, Card, Input, Button, Form } from 'antd';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Box } from '@components';
import BoxModal from '../box';
import { formatRupiah } from '@libs/number';
import { submitOffer, reset } from '@redux/reducers/offer';
const { Text } = Typography;
const { Meta } = Card;

const SubmitOffer = ({ product }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [isShow, setShow] = useState(false);

  const { loading: offerLoading, success } = useSelector((state) => state.offer.submitOffer);

  useEffect(() => {
    if (success) {
      setShow(false);
      form.resetFields();
      dispatch(reset());
      navigate(`/${product.slug}`, { replace: true });
    }
  }, [success]); // eslint-disable-line

  const onFinish = async (values) => {
    console.log(values);
    dispatch(
      submitOffer({
        id: product?.id,
        ...values
      })
    );
  };
  return (
    <>
      <Button
        type="primary"
        block
        size="large"
        className="modal-trigger"
        onClick={() => setShow(true)}
      >
        Saya tertarik dan ingin nego
      </Button>
      <BoxModal
        Title="Masukan Harga Tawarmu"
        className="modal-submit-offer"
        isShow={isShow}
        onCancel={() => setShow(false)}
      >
        <Text type="secondary">
          Harga tawaranmu akan diketahui penjual, jika penjual cocok kamu akan segera dihubungi
          penjual.
        </Text>
        <Card style={{ width: '100%' }}>
          <Meta
            avatar={<Avatar src={product?.images[0]?.url || 'https://picsum.photos/200/300'} />}
            title={product?.name || 'Jam Tangan'}
            description={formatRupiah(product?.price || 250000)}
          />
        </Card>
        <Box className="input-offer">
          <Form form={form} layout="vertical" autoComplete="off" onFinish={onFinish}>
            <Text>Harga Tawar</Text>
            <Form.Item
              name="offer_price"
              rules={[
                {
                  required: true,
                  message: 'Harga penawaran harus diisi'
                }
              ]}
            >
              <Input placeholder="Rp 0,00" />
            </Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              size="large"
              className="my-[15px] button-offer"
              block
              disabled={offerLoading}
              loading={offerLoading}
            >
              Kirim
            </Button>
          </Form>
        </Box>
      </BoxModal>
    </>
  );
};

export default SubmitOffer;
