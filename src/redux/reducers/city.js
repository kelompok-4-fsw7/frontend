import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import config from '@/config';

export const getCities = createAsyncThunk('cities', async () => {
  try {
    const response = await axios.get(`${config.API_URL}/${config.API_VERSION}/cities`);
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

const initialState = {
  cities: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  }
};

export const citySlice = createSlice({
  name: 'city',
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: {
    // CITY
    [getCities.pending]: (state) => {
      state.cities.loading = true;
    },
    [getCities.fulfilled]: (state, action) => {
      state.cities.data = action.payload.data.data;
      state.cities.success = true;
      state.cities.error = false;
      state.cities.errorMessage = null;
      state.cities.loading = false;
    },
    [getCities.rejected]: (state, action) => {
      state.cities.success = false;
      state.cities.error = action.error.message;
      state.cities.errorMessage = action.payload.message;
      state.cities.loading = false;
    }
  }
});

export const { reset } = citySlice.actions;

export default citySlice.reducer;
