import { useRef } from 'react';
import axios from 'axios';
import config from '@/config';

export const useUpload = () => {
  const inputRef = useRef();

  const handleUpload = async (event, folder = 'users') => {
    try {
      const file = event.target.files[0];
      const payload = new FormData();
      payload.append('folder', folder);
      payload.append('image', file);

      const { data: res } = await axios.post(
        `${config.API_URL}/${config.API_VERSION}/upload`,
        payload
      );

      return res;
    } catch (error) {
      console.log(error.toString());
    }
  };

  const onClick = () => {
    inputRef.current?.click();
  };

  return {
    inputRef,
    handleUpload,
    onClick
  };
};
