import { Helmet } from 'react-helmet';
import { Typography } from 'antd';
import { Link } from 'react-router-dom';
import { useEffect } from 'react';
import { Button, Form, Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@components';
import { forgetPassword } from '@redux/reducers/password';

const { Paragraph, Title } = Typography;

function ForgotPassword() {
  const { success, loading } = useSelector((state) => state.password.forget);
  const dispatch = useDispatch();

  const onFinish = async (values) => {
    dispatch(forgetPassword(values));
  };

  const [form] = Form.useForm();

  useEffect(() => {
    if (success === true) {
      form.resetFields();
    }
  }, [success]); // eslint-disable-line

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
      <Title className="layout-auth-title" level={3}>
        Lupa Password
      </Title>
      <Form
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          name="email"
          label="Email"
          rules={[
            {
              required: true,
              message: 'Silakan masukkan email Anda'
            },
            { type: 'email', message: 'Email tidak valid' }
          ]}
        >
          <Input placeholder="Contoh: johndee@gmail.com" />
        </Form.Item>

        <Form.Item>
          <Button
            loading={loading}
            disabled={loading}
            htmlType="submit"
            type="primary"
            block
            size="large"
          >
            Send Email Verification
          </Button>
        </Form.Item>
      </Form>

      <Box className="layout-auth-info">
        <Paragraph>
          Kembali ke halaman{' '}
          <Link to="/login">
            <strong>login</strong>
          </Link>
        </Paragraph>
      </Box>

      <Helmet>
        <title>Lupa Password</title>
      </Helmet>
    </>
  );
}

export default ForgotPassword;
