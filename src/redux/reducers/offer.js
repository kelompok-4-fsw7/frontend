import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import config from '@/config';
import { getToken } from './auth';
import useToast from '@libs/toast';

export const getOfferStatusByProdustAndUser = createAsyncThunk('offer/status', async (id) => {
  try {
    const response = await axios.get(
      `${config.API_URL}/${config.API_VERSION}/product/${id}/offer`,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const submitOffer = createAsyncThunk('offer/submit', async (values) => {
  const toast = useToast();
  try {
    const response = await axios.post(
      `${config.API_URL}/${config.API_VERSION}/product/${values.id}/offer`,
      {
        offer_price: values.offer_price
      },
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 201) {
      toast('Harga tawarmu berhasil dikirim ke penjual', 'success');
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const updateOffer = createAsyncThunk('updateOffer', async (values) => {
  const toast = useToast();
  try {
    const response = await axios.put(
      `${config.API_URL}/${config.API_VERSION}/product/${values.id}/offer`,
      {
        status: values.status
      },
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 202) {
      toast('Success update offer', 'success');
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const updateProductOffer = createAsyncThunk('updateProductOffer', async (values) => {
  const toast = useToast();
  try {
    const response = await axios.put(
      `${config.API_URL}/${config.API_VERSION}/product/${values.product_id}/offer/${values.offer_id}`,
      {
        status: values.status
      },
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 202) {
      toast('Success update product', 'success');
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getOffersByUser = createAsyncThunk('user/offer/made', async (id) => {
  try {
    const response = await axios.get(`${config.API_URL}/${config.API_VERSION}/user/offer/made`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getOffersBySeller = createAsyncThunk('user/offer/received', async (id) => {
  try {
    const response = await axios.get(
      `${config.API_URL}/${config.API_VERSION}/user/offer/received`,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getOffer = createAsyncThunk('getOffer', async (id) => {
  try {
    const response = await axios.get(`${config.API_URL}/${config.API_VERSION}/user/offer/${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

const initialState = {
  offerStatus: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  submitOffer: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  offerByProductOwner: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  offersMade: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  offersReceived: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  offer: {
    loading: true,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  updateOfferStatus: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  updateProductOffer: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  }
};

export const offerSlice = createSlice({
  name: 'offer',
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: {
    // OFFER STATUS
    [getOfferStatusByProdustAndUser.pending]: (state) => {
      state.offerStatus.loading = true;
    },
    [getOfferStatusByProdustAndUser.fulfilled]: (state, action) => {
      state.offerStatus.data = action.payload.data.data;
      state.offerStatus.success = true;
      state.offerStatus.error = false;
      state.offerStatus.errorMessage = null;
      state.offerStatus.loading = false;
    },
    [getOfferStatusByProdustAndUser.rejected]: (state, action) => {
      state.offerStatus.success = false;
      state.offerStatus.error = action.error.message;
      state.offerStatus.errorMessage = action.payload.message;
      state.offerStatus.loading = false;
    },
    // OFFER CREATE
    [submitOffer.pending]: (state) => {
      state.submitOffer.loading = true;
    },
    [submitOffer.fulfilled]: (state, action) => {
      state.submitOffer.data = action.payload.data.data;
      state.submitOffer.success = true;
      state.submitOffer.error = false;
      state.submitOffer.errorMessage = null;
      state.submitOffer.loading = false;
    },
    [submitOffer.rejected]: (state, action) => {
      state.submitOffer.success = false;
      state.submitOffer.error = action.error.message;
      state.submitOffer.errorMessage = action.payload.message;
      state.submitOffer.loading = false;
    },
    // OFFER UPDATE
    [updateOffer.pending]: (state) => {
      state.updateOfferStatus.loading = true;
    },
    [updateOffer.fulfilled]: (state, action) => {
      state.updateOfferStatus.success = true;
      state.updateOfferStatus.error = false;
      state.updateOfferStatus.errorMessage = null;
      state.updateOfferStatus.loading = false;
    },
    [updateOffer.rejected]: (state, action) => {
      state.updateOfferStatus.success = false;
      state.updateOfferStatus.error = action.error.message;
      state.updateOfferStatus.errorMessage = action.payload.message;
      state.updateOfferStatus.loading = false;
    },

    // OFFER UPDATE
    [updateProductOffer.pending]: (state) => {
      state.updateProductOffer.loading = true;
    },
    [updateProductOffer.fulfilled]: (state, action) => {
      state.updateProductOffer.success = true;
      state.updateProductOffer.error = false;
      state.updateProductOffer.errorMessage = null;
      state.updateProductOffer.loading = false;
    },
    [updateProductOffer.rejected]: (state, action) => {
      state.updateProductOffer.success = false;
      state.updateProductOffer.error = action.error.message;
      state.updateProductOffer.errorMessage = action.payload.message;
      state.updateProductOffer.loading = false;
    },

    // OFFERS MADE
    [getOffersByUser.pending]: (state) => {
      state.offersMade.loading = true;
    },
    [getOffersByUser.fulfilled]: (state, action) => {
      state.offersMade.data = action.payload.data.data;
      state.offersMade.success = true;
      state.offersMade.error = false;
      state.offersMade.errorMessage = null;
      state.offersMade.loading = false;
    },
    [getOffersByUser.rejected]: (state, action) => {
      state.offersMade.success = false;
      state.offersMade.error = action.error.message;
      state.offersMade.errorMessage = action.payload.message;
      state.offersMade.loading = false;
    },
    // OFFERS RECEIVED
    [getOffersBySeller.pending]: (state) => {
      state.offersReceived.loading = true;
    },
    [getOffersBySeller.fulfilled]: (state, action) => {
      state.offersReceived.data = action.payload.data.data;
      state.offersReceived.success = true;
      state.offersReceived.error = false;
      state.offersReceived.errorMessage = null;
      state.offersReceived.loading = false;
    },
    [getOffersBySeller.rejected]: (state, action) => {
      state.offersReceived.success = false;
      state.offersReceived.error = action.error.message;
      state.offersReceived.errorMessage = action.payload.message;
      state.offersReceived.loading = false;
    },
    // OFFER
    [getOffer.pending]: (state) => {
      state.offer.loading = true;
    },
    [getOffer.fulfilled]: (state, action) => {
      state.offer.data = action.payload.data.data;
      state.offer.success = true;
      state.offer.error = false;
      state.offer.errorMessage = null;
      state.offer.loading = false;
    },
    [getOffer.rejected]: (state, action) => {
      state.offer.success = false;
      state.offer.error = action.error.message;
      state.offer.errorMessage = action.payload.message;
      state.offer.loading = false;
    }
  }
});

export const { reset } = offerSlice.actions;

export default offerSlice.reducer;
