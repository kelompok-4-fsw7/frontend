import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Home from '@pages/home';
import Product from '@pages/product';
import Login from '@pages/auth/login';
import Confirm from '@pages/auth/confirm';
import Activate from '@pages/auth/activate';
import Register from '@pages/auth/register';
import ForgotPassword from '@pages/auth/password/forgot';
import ResetPassword from '@pages/auth/password/reset';
import Profile from '@pages/user/account/edit';
import SaleList from '@pages/user/sale-list/product';
import Diminati from '@pages/user/sale-list/diminati';
import Terjual from '@pages/user/sale-list/terjual';
import Transaksi from '@pages/user/sale-list/transaksi';
import Penawaran from '@pages/user/sale-list/penawaran';
import Notifications from '@pages/notifications';
import EditProduct from '@pages/user/product/edit';
import CreateProduct from '@pages/user/product/create';
import Account from '@pages/user/account';
import ResendVerification from '@pages/auth/confirm/resend';
import AccountSettings from '@pages/user/account/settings';
import Offer from '@pages/user/offer';

import { AuthLayout, GeneralLayout, AppLayout, SaleLayout, ProductLayout } from '@components';
import { Authenticated, Guest, UserValid } from '@libs/middleware';
import ScrollTop from '@libs/scroll';

import '@fontsource/poppins/300.css';
import '@fontsource/poppins/400.css';
import '@fontsource/poppins/500.css';
import '@fontsource/poppins/600.css';
import '@fontsource/poppins/700.css';

function App() {
  return (
    <>
      <BrowserRouter>
        <ScrollTop />
        <Routes>
          <Route>
            <Route
              element={
                <Guest>
                  <AuthLayout />
                </Guest>
              }
            >
              <Route exact path="/login" element={<Login />} />
              <Route exact path="/activate" element={<Activate />} />
              <Route exact path="/confirm/:token" element={<Confirm />} />
              <Route exact path="/register" element={<Register />} />
              <Route exact path="/forgot-password" element={<ForgotPassword />} />
              <Route exact path="/reset-password" element={<ResetPassword />} />
              <Route exact path="/confirm/resend" element={<ResendVerification />} />
            </Route>
            <Route element={<GeneralLayout />}>
              <Route exact path="/" element={<Home />} />
            </Route>
            <Route element={<ProductLayout />}>
              <Route exact path="/:slug" element={<Product />} />
            </Route>
            <Route
              element={
                <Authenticated>
                  <AppLayout />
                </Authenticated>
              }
            >
              <Route exact path="/user/account" element={<Account />} />
              <Route exact path="/user/account/settings" element={<AccountSettings />} />
              <Route exact path="/user/account/edit" element={<Profile />} />
              <Route exact path="/user/notifications" element={<Notifications />} />
              <Route exact path="/user/offer/:id" element={<Offer />} />
            </Route>
            <Route
              element={
                <Authenticated>
                  <UserValid>
                    <AppLayout />
                  </UserValid>
                </Authenticated>
              }
            >
              <Route exact path="/user/product/:id/edit" element={<EditProduct />} />
              <Route exact path="/user/product/create" element={<CreateProduct />} />
            </Route>
            <Route
              element={
                <Authenticated>
                  <UserValid>
                    <SaleLayout />
                  </UserValid>
                </Authenticated>
              }
            >
              <Route exact path="/user/daftar-jual" element={<SaleList />} />
              <Route exact path="/user/diminati" element={<Diminati />} />
              <Route exact path="/user/terjual" element={<Terjual />} />
              <Route exact path="/user/penawaran" element={<Penawaran />} />
              <Route exact path="/user/transaksi" element={<Transaksi />} />
            </Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
