import BoxModal from '../box';
import { Typography, Radio, Space, Button } from 'antd';
import { BsWhatsapp } from 'react-icons/bs';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateProductOffer, reset } from '@redux/reducers/offer';
const { Text } = Typography;

export default function Index({ offer, isShow, onCancel }) {
  const [value, setValue] = useState();
  const [button, setButton] = useState(true);
  const dispatch = useDispatch();

  const { loading: updateProductOfferLoading, success } = useSelector(
    (state) => state.offer.updateProductOffer
  );

  const onChange = (e) => {
    setValue(e.target.value);
    setButton(false);
  };

  const onSubmit = () => {
    dispatch(
      updateProductOffer({
        offer_id: offer.id,
        product_id: offer.product_id,
        status: value
      })
    );
  };
  return (
    <>
      <BoxModal
        Title={<div className="mb-6">Perbaharui status penjualan produkmu</div>}
        ButtonText="Kirim"
        className="modal-status"
        button={button}
        ModalText="Hubungi di  "
        ModalIcon={<BsWhatsapp />}
        isShow={isShow}
        onCancel={() => onCancel()}
      >
        <Radio.Group onChange={onChange} value={value} className="antialiased">
          <Space direction="vertical">
            <Radio value={2}>
              <div className="text-radio-button mb-6">
                <Text className="title-radio">Berhasil Terjual</Text>
                <Text className="desc-radio">
                  Kamu telah sepakat menjual produk ini kepada pembeli
                </Text>
              </div>
            </Radio>
            <Radio value={0}>
              <div className="text-radio-button mb-8">
                <Text className="title-radio">Batalkan Transaksi</Text>
                <Text className="desc-radio">
                  Kamu membatalkan transaksi produk ini dengan pembeli
                </Text>
              </div>
            </Radio>
          </Space>
        </Radio.Group>
        <Button
          type="primary"
          htmlType="submit"
          size="large"
          block
          className="button-offer"
          loading={updateProductOfferLoading}
          disabled={button}
          onClick={() => onSubmit()}
        >
          Kirim
        </Button>
      </BoxModal>
    </>
  );
}
