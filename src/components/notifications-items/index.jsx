import { Avatar, Card, Typography } from 'antd';
import { useNavigate } from 'react-router-dom';
import dateFormat from 'dateformat';
import { useDispatch } from 'react-redux';
import { updateRead } from '@redux/reducers/notification';
import { Box } from '@components';
import { formatRupiah } from '@libs/number';

const { Meta } = Card;
const { Text } = Typography;

function NotificationsItem({ notification }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const navigateItem = () => {
    dispatch(
      updateRead({
        id: notification?.id
      })
    );
    return navigate(notification?.route, { replace: true });
  };

  return (
    <Card className="notification-item" onClick={() => navigateItem()}>
      <Meta
        avatar={
          <Avatar
            src={
              notification.type === 1
                ? notification?.payload?.images[0]?.url
                : [2, 3, 4, 5].includes(notification.type)
                ? notification?.payload?.product?.images[0]?.url
                : null
            }
          />
        }
      />
      <Box className="notification-item-description">
        <Box className="top">
          <Text type="secondary">
            {notification.type === 1
              ? 'Berhasil diterbitkan'
              : notification.type === 2
              ? 'Penawaran produk'
              : notification.type === 3
              ? 'Penawaran ditolak'
              : notification.type === 4
              ? 'Penawaran diterima'
              : notification.type === 5
              ? 'Pembelian berhasil'
              : null}
          </Text>
          <Text type="secondary flex items-center">
            {dateFormat(notification?.created_at, 'dd mmm, HH:MM')}
            {!notification?.is_read && (
              <span className="h-2 w-2 rounded bg-red-600 block ml-2"></span>
            )}
          </Text>
        </Box>
        <Box className="middle">
          {notification?.type === 1 ? (
            <>
              <Text>{notification?.payload?.name}</Text>
              <Text>{formatRupiah(notification?.payload?.price || 0)}</Text>
            </>
          ) : [2, 3, 4, 5].includes(notification?.type) ? (
            <>
              <Text>{notification?.payload?.product?.name}</Text>

              <Text>{formatRupiah(notification?.payload?.product?.price || 0)}</Text>
              <Text>Ditawar {formatRupiah(notification?.payload?.offer_price || 0)}</Text>
            </>
          ) : null}
        </Box>
        {notification.type === 4 && (
          <Box className="top">
            <Text type="secondary">Kamu akan segera dihubungi penjual via whatsapp</Text>
          </Box>
        )}
      </Box>
    </Card>
  );
}

export default NotificationsItem;
