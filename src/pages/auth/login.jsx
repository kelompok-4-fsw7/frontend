import { Helmet } from 'react-helmet';
import { useEffect } from 'react';
import { Typography } from 'antd';
import { Link, useNavigate } from 'react-router-dom';
import { Button, Form, Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@components';
import { login, getToken } from '@redux/reducers/auth';

const { Paragraph, Title } = Typography;

function Login() {
  const { token, loading } = useSelector((state) => state.auth.login);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onFinish = async (values) => {
    dispatch(login(values));
  };

  const [form] = Form.useForm();

  useEffect(() => {
    if (getToken() !== null) {
      form.resetFields();
      navigate('/', { replace: true });
    }
  }, [token]); // eslint-disable-line

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
      <Box className="layout-auth-form-wrapper">
        <Title className="layout-auth-title" level={3}>
          Masuk
        </Title>
        <Form
          form={form}
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            name="email"
            label="Email"
            rules={[
              {
                required: true,
                message: 'Silakan masukkan email Anda'
              },
              { type: 'email', message: 'Email tidak valid' }
            ]}
          >
            <Input placeholder="Contoh: johndee@gmail.com" />
          </Form.Item>
          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: 'Silakan masukkan password Anda'
              }
            ]}
          >
            <Input.Password placeholder="Masukkan password" />
          </Form.Item>
          <Form.Item>
            <Link to="/forgot-password">Lupa Password</Link>
          </Form.Item>
          <Form.Item>
            <Button
              loading={loading}
              disabled={loading}
              htmlType="submit"
              type="primary"
              block
              size="large"
            >
              Masuk
            </Button>
          </Form.Item>
        </Form>
      </Box>
      <Box className="layout-auth-info">
        <Paragraph>
          Belum punya akun?{' '}
          <Link to="/register">
            <strong>Daftar di sini</strong>
          </Link>
        </Paragraph>
      </Box>

      <Helmet>
        <title>Login</title>
      </Helmet>
    </>
  );
}

export default Login;
