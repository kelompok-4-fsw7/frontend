import { Button, Skeleton } from 'antd';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, ModalSubmitOffer } from '@components';
import { formatRupiah } from '@libs/number';
import { getToken } from '@redux/reducers/auth';
import { handleLike } from '@redux/reducers/product_like';

const ProductDetail = ({ loading = false, product, user, canBuy = true }) => {
  const dispatch = useDispatch();
  const [liked, setLiked] = useState(true);
  const isLogin = getToken() !== null;
  const { loading: handleLikeLoading } = useSelector((state) => state.product_like.like);
  const { data: likeStatus, loading: likeStatusLoading } = useSelector(
    (state) => state.product_like.getStatus
  );

  useEffect(() => {
    if (likeStatus === 1) {
      setLiked(false);
    } else {
      setLiked(true);
    }
  }, [likeStatus]);

  return loading ? (
    <Skeleton.Button className="skeleton-product-detail mb-6" active />
  ) : (
    <Box className="product-detail">
      <Box className="product-detail-text">
        <p className="product-detail-name">{product?.name}</p>
        <p className="product-detail-category">{product?.category?.name}</p>
        <p className="product-detail-price">{formatRupiah(product?.price)}</p>
      </Box>
      {isLogin ? (
        <>
          {user?.id === product?.user_id ? (
            <>
              {product?.status === 0 && (
                <Link to={`/user/product/${product.id}/edit`}>
                  <Button type="primary" size="large" className="mb-[14px]" block>
                    Terbitkan
                  </Button>
                </Link>
              )}
              {product?.status !== 2 && (
                <Link to={`/user/product/${product.id}/edit`}>
                  <Button type="primary" ghost size="large" block>
                    Edit
                  </Button>
                </Link>
              )}
              {product?.status === 2 && (
                <Button type="danger" size="large" block>
                  Produk sudah laku
                </Button>
              )}
            </>
          ) : (
            <>
              {canBuy ? (
                user?.status < 2 ? (
                  <Link to="/user/account/edit">
                    <Button type="primary" size="large" block>
                      Saya tertarik dan ingin nego
                    </Button>
                  </Link>
                ) : (
                  <ModalSubmitOffer product={product} />
                )
              ) : (
                <>
                  <Button type="primary" disabled size="large" block>
                    Menunggu Respon Penjual
                  </Button>
                </>
              )}
              <Button
                ghost
                type="primary"
                className="mt-2"
                size="large"
                block
                disabled={handleLikeLoading || likeStatusLoading}
                loading={handleLikeLoading || likeStatusLoading}
                onClick={() => {
                  dispatch(handleLike(product?.id));
                  setLiked(!liked);
                }}
              >
                {liked ? 'Suka' : 'Batal Suka'}
              </Button>
            </>
          )}
        </>
      ) : (
        <Link to="/login">
          <Button type="primary" size="large" block>
            Saya tertarik dan ingin nego
          </Button>
        </Link>
      )}
    </Box>
  );
};

export default ProductDetail;
