import { Outlet } from 'react-router-dom';
import { Box, Header } from '@components';

const LayoutApp = () => {
  return (
    <Box className="layout-app">
      <Header type="app" />
      <Outlet />
    </Box>
  );
};

export default LayoutApp;
