# Frontend Secondhand V1

## Tools

- Visual Studio Code

## Extension Visual Studio Code

- Prettier - Code formatter
- GitLens — Git supercharged
- EditorConfig for VS Code
- ES7+ React/Redux/React-Native snippets
- Tailwind

## Cara Menjalankan

1. Clone repository ini dengan menggunakan perintah `git clone https://{username_gitlab}@gitlab.com/kelompok-4-fsw7/frontend.git` pada terminal / command line.
2. Pastikan folder project sudah aktif pada terminal / command line.
3. Jalankan perintah `yarn install`
4. Jalankan perintah `yarn prepare`
5. Jalankan perintah `yarn start`
6. Happy Hacking
