import { Box } from '@components';
import { ReactComponent as FacebookIcon } from '@assets/svg/facebook.svg';
import { ReactComponent as TwitterIcon } from '@assets/svg/twitter.svg';
import { ReactComponent as ShopeeIcon } from '@assets/svg/shopee.svg';
import { ReactComponent as InstagramIcon } from '@assets/svg/instagram.svg';
import { ReactComponent as WhatsappIcon } from '@assets/svg/whatsapp.svg';
import { ReactComponent as GmailIcon } from '@assets/svg/gmail.svg';

const Footer = () => {
  return (
    <Box className="footer">
      <Box className="container max-w-[1168px]">
        <Box className="copyright">
          <Box>Secondhand @Copyright 2022</Box>
        </Box>
        <Box className="social-media grid">
          <a href="#!">
            <FacebookIcon />
          </a>
          <a href="#!">
            <TwitterIcon />
          </a>
          <a href="#!" className="mr-6 text-gray-600">
            <ShopeeIcon />
          </a>
          <a href="#!" className="mr-6 text-gray-600">
            <InstagramIcon />
          </a>
          <a href="#!" className="mr-6 text-gray-600">
            <WhatsappIcon />
          </a>
          <a href="#!" className="text-gray-600">
            <GmailIcon />
          </a>
        </Box>
      </Box>
    </Box>
  );
};

export default Footer;
