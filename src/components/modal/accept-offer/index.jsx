import { Typography, Card, Avatar, Button } from 'antd';
import React from 'react';
import { BsWhatsapp } from 'react-icons/bs';
import { formatRupiah } from '@libs/number';

import BoxModal from '../box';

const { Text } = Typography;
const { Meta } = Card;

const AcceptOffer = ({ offer, isShow, onCancel }) => {
  return (
    <>
      <BoxModal
        Title="Yeay kamu berhasil mendapat harga yang sesuai"
        Icon={<BsWhatsapp className="icon-button-offer" />}
        ButtonText="Hubungi via Whatsapp"
        className="modal-accept-offer"
        ModalText="Terima"
        isShow={isShow}
        onCancel={() => onCancel()}
      >
        <Text type="secondary" className="mb-2">
          Segera hubungi pembeli melalui WhatsApp untuk transaksi selanjutnya
        </Text>

        <Card className="card-accept-offer">
          <Text className="text-center font-medium mb-2 block">Product Match</Text>
          <Meta
            avatar={<Avatar src={offer?.user?.image || `https://joeschmoe.io/api/v1/random`} />}
            title={offer?.user?.name || 'Nama Pembeli'}
            description={offer?.user?.city?.name || 'Kota'}
          />
          <Meta
            avatar={
              <Avatar
                src={offer?.product?.images[0]?.url || 'https://joeschmoe.io/api/v1/random'}
              />
            }
            title={offer?.product?.name || 'Jam Tangan Casio'}
            className="accept-offer-card"
            description={
              <>
                <Text className="not-accepted-price">
                  {formatRupiah(offer?.product?.price || 250000)}
                </Text>
                <Text>Ditawar {formatRupiah(offer?.offer_price || 250000)}</Text>
              </>
            }
          />
        </Card>
        <a href={`https://wa.me/${offer?.user?.phone}`} target="_blank" rel="noreferrer">
          <Button type="primary" htmlType="submit" size="large" block className="button-offer">
            Hubungi via Whatsapp <BsWhatsapp className="icon-button-offer" />
          </Button>
        </a>
      </BoxModal>
    </>
  );
};

export default AcceptOffer;
