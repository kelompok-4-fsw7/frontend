import { Box } from '@components';
import { Avatar, Card, Skeleton } from 'antd';
const { Meta } = Card;

const UserCard = ({ loading = false, user, button }) => {
  return (
    <Box>
      {loading ? (
        <Skeleton.Button className="skeleton-user-card" active />
      ) : (
        <Card className="user-card">
          <Meta
            avatar={
              <Avatar
                className="user-img"
                src={user?.image || 'https://joeschmoe.io/api/v1/random'}
              />
            }
            title={user?.name || 'Nama User'}
            description={user?.city?.name || 'Kota'}
            className="image"
          />
          <div className="devider"></div>
          {button && button}
        </Card>
      )}
    </Box>
  );
};

export default UserCard;
