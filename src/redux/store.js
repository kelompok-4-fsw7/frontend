import { configureStore } from '@reduxjs/toolkit';
import authSlice from '@redux/reducers/auth';
import categorySlice from '@redux/reducers/category';
import productSlice from '@redux/reducers/product';
import citySlice from '@redux/reducers/city';
import offerSlice from '@redux/reducers/offer';
import passwordSlice from '@redux/reducers/password';
import productLikeSlice from '@redux/reducers/product_like';
import transactionSlice from '@redux/reducers/transaction';
import notificationSlice from '@redux/reducers/notification';

export const store = configureStore({
  reducer: {
    auth: authSlice,
    category: categorySlice,
    product: productSlice,
    city: citySlice,
    offer: offerSlice,
    password: passwordSlice,
    product_like: productLikeSlice,
    transaction: transactionSlice,
    notification: notificationSlice
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false
    })
});
