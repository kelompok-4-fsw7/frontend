import { FiPlus } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import { Box } from '@components';

const ProductAdd = () => {
  return (
    <Link to={'/user/product/create'} className="link-product-add">
      <Box>
        <FiPlus size={24} className="create-product-icon" />
        <span>Tambah Produk</span>
      </Box>
    </Link>
  );
};

export default ProductAdd;
