import { Helmet } from 'react-helmet';
import { useEffect, useState } from 'react';
import { Skeleton, Pagination, Button } from 'antd';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { FiPlus } from 'react-icons/fi';
import { Box, Empty, ProductCard, Slider, Category } from '@components';
import { getCategories } from '@redux/reducers/category';
import { getProducts, getProductsByName } from '@redux/reducers/product';
import useQuery from '@libs/query';

function Home() {
  const [categoryActive, setCategory] = useState(null);
  const [page, setPage] = useState(1); // eslint-disable-line
  let query = useQuery();
  const name = query.get('name');

  const dispatch = useDispatch();
  const { data: categories, loading: categoriesLoading } = useSelector(
    (state) => state.category.category
  );
  const {
    data: products,
    loading: productsLoading,
    pagination: productsPagination,
    isSearch: productsSearch
  } = useSelector((state) => state.product.product);

  useEffect(() => {
    dispatch(getCategories());
    dispatch(
      getProductsByName({
        name: name,
        page: 1
      })
    );
  }, []); // eslint-disable-line

  useEffect(() => {
    if (productsSearch) {
      setCategory(null);
    }
  }, [productsSearch]); // eslint-disable-line

  const changeCategory = (categoryID) => {
    setCategory(categoryID);
    dispatch(
      getProducts({
        category_id: categoryID,
        page: 1
      })
    );
  };

  const changePage = (pageID) => {
    setPage(pageID);
    dispatch(
      getProducts({
        category_id: categoryActive,
        page: pageID
      })
    );
  };

  console.log(productsPagination);

  return (
    <>
      <Slider />
      <Box className="container max-w-[1168px] -mt-[148px] z-10 md:mt-0 relative">
        <h2 className="mb-4 text-base font-bold">Telusuri Kategori</h2>
        <Box className="category-container">
          {categoriesLoading ? (
            <>
              {Array(6)
                .fill(1)
                .map((i) => (
                  <Skeleton.Button key={i} className="skeleton-category" active />
                ))}
            </>
          ) : (
            <Category
              onClick={() => {
                changeCategory(null);
              }}
              category={{ name: 'Semua' }}
              isActive={categoryActive === null}
            />
          )}

          {!categoriesLoading &&
            categories?.map((category) => (
              <Category
                category={category}
                isActive={categoryActive === category?.id}
                onClick={() => {
                  changeCategory(category?.id);
                }}
              />
            ))}
        </Box>
      </Box>
      <Box className="container max-w-[1168px]">
        <Box className="product-grid">
          {productsLoading && (
            <>
              {Array(6)
                .fill(1)
                .map((i) => (
                  <Skeleton key={i} className="skeleton-product" active />
                ))}
            </>
          )}
          {!productsLoading &&
            products.map((product, i) => <ProductCard product={product} key={i}></ProductCard>)}
          {!productsLoading && products?.length < 1 && (
            <Box className="col-span-6">
              <Empty className="mx-auto" description={'Produk tidak ditemukan'} />
            </Box>
          )}
        </Box>

        {!productsLoading && productsPagination && productsPagination?.page_count > 1 && (
          <Box className="md:pb-10">
            <Pagination
              defaultCurrent={productsPagination?.page}
              defaultPageSize={productsPagination?.page_size}
              total={productsPagination?.count}
              onChange={(page) => {
                changePage(page);
              }}
            />
          </Box>
        )}
      </Box>
      <Link to="/user/product/create" className="button-add-product">
        <Button type="primary" size="large" block>
          <FiPlus className="inline-block mr-2" size={20} /> Jual
        </Button>
      </Link>
      <Helmet>
        <title>Home</title>
      </Helmet>
    </>
  );
}

export default Home;
