import { Skeleton } from 'antd';
import { Box } from '@components';

const ProductDescription = ({ loading = false, product }) => {
  return loading ? (
    <Skeleton.Button className="skeleton-product-description mb-6" active />
  ) : (
    <Box className="description-wrapper">
      <h1 className="description-title">Deskripsi</h1>
      <div
        className="description-text"
        dangerouslySetInnerHTML={{ __html: product?.description }}
      />
    </Box>
  );
};

export default ProductDescription;
