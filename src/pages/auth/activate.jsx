import { Helmet } from 'react-helmet';
import { Typography } from 'antd';
import { Link } from 'react-router-dom';
import { Button, Row, Col } from 'antd';
import { Box } from '@components';

const { Paragraph, Title } = Typography;

function Activate() {
  return (
    <>
      <Box className="layout-auth-form-wrapper">
        <Title className="layout-auth-title" level={3}>
          Aktivasi Akun
        </Title>
        <Paragraph>
          Selamat Datang! Anda terdaftar di Secondhand. Silakan cek Email Anda di Inbox, Spam atau
          Promotion untuk Aktivasi Akun Anda.{' '}
        </Paragraph>
        <Row gutter={16}>
          <Col span={12}>
            <Link to="/login">
              <Button block type="primary" size="large">
                Login
              </Button>
            </Link>
          </Col>
          <Col span={12}>
            <Link to="/confirm/resend">
              <Button block type="primary" size="large" ghost>
                Kirim Ulang
              </Button>
            </Link>
          </Col>
        </Row>
      </Box>

      <Helmet>
        <title>Aktivasi Akun</title>
      </Helmet>
    </>
  );
}

export default Activate;
