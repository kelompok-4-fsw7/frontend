import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import config from '@/config';
import useToast from '@libs/toast';
import { getToken } from './auth';

export const getStatusLike = createAsyncThunk('getStatusLike', async (productID) => {
  try {
    const response = await axios.get(
      `${config.API_URL}/${config.API_VERSION}/product/${productID}/like`,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getProductLiked = createAsyncThunk('getProductLiked', async () => {
  try {
    const response = await axios.get(
      `${config.API_URL}/${config.API_VERSION}/user/products-liked`,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const handleLike = createAsyncThunk('handleLike', async (productID) => {
  const toast = useToast();
  try {
    const response = await axios.post(
      `${config.API_URL}/${config.API_VERSION}/product/${productID}/like`,
      {},
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 200) {
      toast(response.data.message, 'success');
      console.log(response.data);
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

const initialState = {
  like: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  liked: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  getStatus: {
    loading: true,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  }
};

export const productLikeSlice = createSlice({
  name: 'product_like',
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: {
    [getStatusLike.pending]: (state) => {
      state.getStatus.loading = true;
      state.getStatus.success = false;
    },
    [getStatusLike.fulfilled]: (state, action) => {
      state.getStatus.data = action.payload.data.data;
      state.getStatus.success = true;
      state.getStatus.error = false;
      state.getStatus.errorMessage = null;
      state.getStatus.loading = false;
    },
    [getStatusLike.rejected]: (state, action) => {
      state.getStatus.success = false;
      state.getStatus.error = action.error.message;
      state.getStatus.errorMessage = action.payload.message;
      state.getStatus.loading = false;
    },
    [handleLike.pending]: (state) => {
      state.like.loading = true;
    },
    [handleLike.fulfilled]: (state, action) => {
      state.like.data = action.payload.data.data;
      state.like.success = true;
      state.like.error = false;
      state.like.errorMessage = null;
      state.like.loading = false;
    },
    [handleLike.rejected]: (state, action) => {
      state.like.success = false;
      state.like.error = action.error.message;
      state.like.errorMessage = action.payload.message;
      state.like.loading = false;
    },
    [getProductLiked.pending]: (state) => {
      state.liked.loading = true;
    },
    [getProductLiked.fulfilled]: (state, action) => {
      state.liked.data = action.payload.data.data;
      state.liked.success = true;
      state.liked.error = false;
      state.liked.errorMessage = null;
      state.liked.loading = false;
    },
    [getProductLiked.rejected]: (state, action) => {
      state.liked.success = false;
      state.liked.error = action.error.message;
      state.liked.errorMessage = action.payload.message;
      state.liked.loading = false;
    }
    // [updateProduct.pending]: (state) => {
    //   state.updateProduct.loading = true;
    // },
    // [updateProduct.fulfilled]: (state, action) => {
    //   state.updateProduct.data = action.payload.data.data;
    //   state.updateProduct.success = true;
    //   state.updateProduct.error = false;
    //   state.updateProduct.errorMessage = null;
    //   state.updateProduct.loading = false;
    // },
    // [updateProduct.rejected]: (state, action) => {
    //   state.updateProduct.success = false;
    //   state.updateProduct.error = action.error.message;
    //   state.updateProduct.errorMessage = action.payload.message;
    //   state.updateProduct.loading = false;
    // }
  }
});

export const { reset } = productLikeSlice.actions;

export default productLikeSlice.reducer;
